resource "hcloud_firewall" "master_firewall" {
  name = var.master_firewall_name

  rule {
    description = "allow 6443 to master"
    direction   = "in"
    protocol    = "tcp"
    port        = "6443"
    source_ips  = [
      "0.0.0.0/0",
      "::/0",
    ]
  }

}

resource "hcloud_firewall" "worker_firewall" {
  name = var.worker_firewall_name

  rule {
    description = "allow ICMP from any"
    direction   = "in"
    protocol    = "icmp"
    source_ips = [
      "0.0.0.0/0",
      "::/0",
    ]
  }

  rule {
    description = "allow ICMP out"
    direction   = "out"
    protocol    = "icmp"
    destination_ips = [
      "0.0.0.0/0",
      "::/0",
    ]
  }

  rule {
    description = "allow SSH from any"
    direction   = "in"
    protocol    = "tcp"
    port        = "22"
    source_ips = [
      "0.0.0.0/0",
      "::/0",
    ]
  }

  rule {
    description = "allow DNS out tcp"
    direction   = "out"
    protocol    = "tcp"
    port        = "53"
    destination_ips = [
      "0.0.0.0/0",
      "::/0",
    ]
  }

  rule {
    description = "allow DNS out udp"
    direction   = "out"
    protocol    = "udp"
    port        = "53"
    destination_ips = [
      "0.0.0.0/0",
      "::/0",
    ]
  }

  rule {
    description = "allow HTTP out"
    direction   = "out"
    protocol    = "tcp"
    port        = "80"
    destination_ips = [
      "0.0.0.0/0",
      "::/0",
    ]
  }

  rule {
    description = "allow HTTPS out"
    direction   = "out"
    protocol    = "tcp"
    port        = "443"
    destination_ips = [
      "0.0.0.0/0",
      "::/0",
    ]
  }

  rule {
    description = "allow NTP out"
    direction   = "out"
    protocol    = "tcp"
    port        = "123"
    destination_ips = [
      "0.0.0.0/0",
      "::/0",
    ]
  }
}
