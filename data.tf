data "external" "hcloud_token" {
  program = ["sh", "-c", <<EOT
echo {\"token\":\"$HCLOUD_TOKEN\"}
EOT
]
}

data "template_file" "master_cloud_config" {
  template  = file("files/master_cloud_config.yml")
  vars      = {
    key               = hcloud_ssh_key.key.public_key,
    k3s_channel       = var.k3s_channel,
    k3s_token         = random_string.k3s_token.result,
    k3s_lb_public_ip  = local.k3s_lb_public_ip,
    k3s_private_ip    = local.master_private_ip,
    count             = count.index,
    hcloud_token      = data.external.hcloud_token.result.token,
    hcloud_network    = hcloud_network.kube.id,
    cluster_cidr      = var.cluster_cidr,
  }

  count               = var.enable_multi_master? 3: 1
}

data "template_file" "node_cloud_config" {
  template  = file("files/node_cloud_config.yml")
  vars      = {
    key             = hcloud_ssh_key.key.public_key,
    k3s_channel     = var.k3s_channel,
    k3s_token       = random_string.k3s_token.result,
    k3s_private_ip  = local.k3s_private_ip,
  }
}

data "kubectl_path_documents" "cluster_autoscaler" {
  pattern = "${path.module}/files/cluster_autoscaler.yml"
  vars    = {
    cluster_autoscaler_version  = var.cluster_autoscaler_version,
    cloud_init                  = base64encode(data.template_file.node_cloud_config.rendered),
    hcloud_network              = hcloud_network.kube.name,
  }
}


#data "external" "kube_config" {
#  program = ["sh", "-c", <<EOT
#echo {\"config\":\"`ssh -T -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${var.cert_private} ubuntu@${hcloud_server.master[0].ipv4_address} cat /etc/rancher/k3s/k3s.yaml | sed -e 's/127.0.0.1/${length(hcloud_load_balancer.master) > 0? hcloud_load_balancer.master[0].ipv4 : hcloud_server.master[0].ipv4_address}/g'`\"}
#EOT
#]
#}

#data "kubectl_file_documents" "cluster_autoscaler" {
#  content = templatefile("files/cluster_autoscaler.yml", {
#    cluster_autoscaler_version  = var.cluster_autoscaler_version
#    cloud_init                  = base64encode(data.template_file.node_cloud_config.rendered),
#    hcloud_network              = hcloud_network.kube.name,
#  })
#}
